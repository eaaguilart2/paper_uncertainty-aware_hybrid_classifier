# -*- coding: utf-8 -*-
from keras.callbacks import Callback
import numpy as np

from keras.models import Model
from keras.layers import Input, RepeatVector, Flatten,Reshape
from keras.engine.topology import Layer
from keras.layers.wrappers import TimeDistributed
from keras import backend as K
from keras.optimizers import Adam
import copy
from keras_wrapper.cnn_model import loadModel

class debug_variables_history(Callback):
    def __init__(self, output_names, output_types, nclasses, store_path, isnormloss):
        self.output_names = output_names
        self.output_types = output_types
        self.nclasses = nclasses
        self.isnormloss = isnormloss
        # Store debug variables.
        self.store_path = store_path
        self.hist_sigma, self.hist_loss,self.hist_dlds, self.hist_loss_s  = [], [], [], []

        for i in range(len(output_names)):
            self.hist_sigma.append([])
            self.hist_loss.append([])
            self.hist_dlds.append([])
            self.hist_loss_s.append([])

    def on_batch_begin(self, epoch, logs={}):
        for i in range(len(self.output_names)):
            logsigma2_list = self.model.get_layer(self.output_names[i]+"_sigma").get_weights()
            sigma_list = np.sqrt(np.exp(logsigma2_list))
            sigma = sigma_list[0][0]
            self.hist_sigma[i].append(str(sigma))

    def on_batch_end(self, epoch, logs={}):
        for i in range(len(self.output_names)):
            #L(W,s)
            loss_s= logs[self.output_names[i]+"_sigma_loss"]
            sigma = float(self.hist_sigma[i][-1])
            output_type_i = self.output_types[i]
            
            c = 1.0 # max error possible (depend of the loss function)
            k = 1.0 # number of labels
            if output_type_i == "categorical" and self.isnormloss: # sl task (categorical cross entropy)
               c = -np.log(np.exp(-1)/(np.exp(1)*self.nclasses[i]))
            elif output_type_i == "binary" and self.isnormloss: # ml task (binary cross entropy)
               c, k = self.nclasses[i], self.nclasses[i]
            elif output_type_i == "binary" and not self.isnormloss:
                k = self.nclasses[i]
                
            # L(W)
            loss = np.power(sigma,2)*(loss_s-((k/c)*np.log(np.power(sigma,2))))
            # Grad Func
            dlds = (-1*np.power(sigma,-2)*loss+(k/c))

            self.hist_dlds[i].append(str(dlds))
            self.hist_loss[i].append(str(loss))
            self.hist_loss_s[i].append(str(loss_s))

    def on_epoch_end(self, epoch, logs={}):
        file_names = [self.store_path+"/sigma.txt", self.store_path+"/loss.txt", 
                      self.store_path+"/dlds.txt", self.store_path+"/loss_s.txt"]
        data_list = [self.hist_sigma, self.hist_loss, self.hist_dlds, self.hist_loss_s]

        # Write current values in the respective debug files.
        for i in range(len(file_names)):
            with open(file_names[i], "a") as f:
                f.write(str(data_list[i])+"\n")

        # Reset debug variables.
        self.hist_sigma, self.hist_loss,self.hist_dlds, self.hist_loss_s  = [], [], [], []

        for i in range(len(self.output_names)):
            self.hist_sigma.append([])
            self.hist_loss.append([])
            self.hist_dlds.append([])
            self.hist_loss_s.append([]) 

############ START EDL Callbacks ################
class update_global_step(Callback):
    def __init__(self, model, store_path, global_step):
        self.global_step = global_step
        self.store_path = store_path
        self.model = model
    def on_batch_end(self, epoch, logs={}):
        curr_global_step = K.get_value(self.global_step)
        K.set_value(self.global_step, curr_global_step + 1)
        #print ("global_step: ", curr_global_step)
    def on_epoch_end(self, epoch, logs={}):
        # serialize weights to HDF5
        self.model.save_weights(self.store_path+'/epoch_'+str(epoch)+'.h5')
        # serialize model to JSON
        model_json = self.model.to_json()
        with open(self.store_path+'/epoch_'+str(epoch)+"_structure.json", "w") as json_file:
            json_file.write(model_json)
        print("Saved model to disk")
########### END EDL Callbacks
