# -*- coding: utf-8 -*-

from keras.callbacks import Callback
import numpy as np

from keras.models import Model
from keras.layers import Input, RepeatVector, Flatten,Reshape
from keras.engine.topology import Layer
from keras.layers.wrappers import TimeDistributed
from keras import backend as K
from keras.optimizers import Adam
import copy
from keras_wrapper.cnn_model import loadModel
from keras_wrapper.dataset import loadDataset

# Take a mean of the results of a TimeDistributed layer.
# Applying TimeDistributedMean()(TimeDistributed(T)(x)) to an
# input of shape (None, ...) returns output of same size.
class TimeDistributedMean(Layer):
	def build(self, input_shape):
		super(TimeDistributedMean, self).build(input_shape)

	# input shape (None, T, ...)
	# output shape (None, ...)
	def compute_output_shape(self, input_shape):
		return (input_shape[0],) + input_shape[2:]

	def call(self, x):
		return K.mean(x, axis=1)
                #return K.std(x, axis=1)


# Apply the predictive entropy function for input with C classes. 
# Input of shape (None, C, ...) returns output with shape (None, ...)
# Input should be predictive means for the C classes.
# In the case of a single classification, output will be (None,).
class PredictiveEntropy(Layer):
	def build(self, input_shape):
		super(PredictiveEntropy, self).build(input_shape)

	# input shape (None, C, ...)
	# output shape (None, ...)
	def compute_output_shape(self, input_shape):
		return (input_shape[0],)

	# x - prediction probability for each class(C)
	def call(self, x):
		return -1 * K.sum(K.log(x+K.epsilon()) * x, axis=1)


class EpistemicPrediction():
    def __init__(self, cnnmodel, dataset, monte_carlo_simulations=100):
        self.cnnmodel = cnnmodel
        self.dataset = dataset
        self.monte_carlo_simulations = monte_carlo_simulations
        self.umodel = self.create_epistemic_uncertainty_model()

    def create_epistemic_uncertainty_model(self):
        if self.monte_carlo_simulations!=1:
            K.set_learning_phase(1)
        else:
            pass
        model = self.cnnmodel.model
        T_monte_carlo_simulations = self.monte_carlo_simulations

	inpt = Input(shape=(model.input_shape[1:]))
        flatten_inpt = Flatten()(inpt)
	x = RepeatVector(T_monte_carlo_simulations)(flatten_inpt)
        x = Reshape([T_monte_carlo_simulations]+list(model.input_shape[1:]))(x)
	# Keras TimeDistributed can only handle a single output from a model :(
	# and we technically only need the softmax outputs.
	hacked_model = Model(inputs=model.inputs, outputs=model.outputs[0])
	x = TimeDistributed(hacked_model, name='epistemic_monte_carlo')(x)
	# predictive probabilities for each class
	softmax_mean = TimeDistributedMean(name='epistemic_softmax_mean')(x)
	variance = PredictiveEntropy(name='epistemic_variance')(softmax_mean)
	epistemic_model = Model(inputs=inpt, outputs=[variance, softmax_mean])

	return epistemic_model

    def predict_one_sample(self):
        model = self.umodel
        model.compile(optimizer=Adam(lr=0.001), loss='categorical_crossentropy', metrics=['categorical_accuracy'])

        print "Calculate epistemic uncertainty (iter/total): "

        X,Y = self.dataset.getXY('test', 2, dataAugmentation=False)
        epistemic_predictions = model.predict(X) # variance, softmax_mean
        return np.mean(epistemic_predictions[0])

    def predict(self):
        model = self.umodel
        model.compile(optimizer=Adam(lr=0.001), loss='categorical_crossentropy', metrics=['categorical_accuracy'])

        batch_size = 6
        if TARGET_SET == 'val':
            total_images = self.dataset.len_val
        elif TARGET_SET == 'test':
            total_images = self.dataset.len_test

        niter = int(np.ceil(total_images/float(batch_size)))

        total_epistemic_predictions = [[],[], [], [], []]
        var_per_class = np.zeros(NCLASSES)
        inst_per_class = np.zeros(NCLASSES)
        print ""
        mean_acc = 0
        eu_x_sample = []
        gts = []
        preds = []
        probs = []
        for i in range(niter):
            print "Calculate epistemic uncertainty (iter/total): "+str(i)+"/"+str(niter)+"\r",
            init_index = i*batch_size
            if i < (niter -1):
                end_index = (i+1)*batch_size
            else:
                end_index = total_images

            X,Y = self.dataset.getXY(TARGET_SET, end_index-init_index, dataAugmentation=False)
            epistemic_predictions = model.predict(X) # variance, softmax_mean
            lbls = np.argmax(Y[0], axis=-1)
            eu_x_sample = eu_x_sample + list(epistemic_predictions[0])
            gts = gts + list(np.argmax(Y[0], -1))
            preds = preds + list(np.argmax(epistemic_predictions[1], -1))
            probs = probs + list(np.max(epistemic_predictions[1], -1))
            mean_acc = mean_acc + np.sum(np.argmax(epistemic_predictions[1], -1) == np.argmax(Y[0], -1))

        mean_var = np.mean(eu_x_sample)
        total_epistemic_predictions[0].append(eu_x_sample)
        total_epistemic_predictions[1].append(mean_acc/float(total_images))
        total_epistemic_predictions[2].append(preds)
        total_epistemic_predictions[3].append(gts)
        total_epistemic_predictions[4].append(probs)
        print ""
        print np.shape(eu_x_sample)
        print mean_var
        print np.min(eu_x_sample)
        print np.max(eu_x_sample)
        print mean_acc/float(total_images)

        return total_epistemic_predictions

if __name__=="__main__":

    GPU_ID = '0'
    TARGET_SET = 'test' # test

    # Configure GPU memory usage for TF
    if 'tensorflow' == K.backend():
        import tensorflow as tf
        from keras.backend.tensorflow_backend import set_session
        config = tf.ConfigProto()
        config.gpu_options.visible_device_list = GPU_ID
        set_session(tf.Session(config=config))

    ### Flat classifier ###
    NCLASSES = 121
    model_folder = 'trained_models/ResNet50_mafood121_all_d_adam_f'
    model_reload_epoch = 24
    dataset_filepath = 'datasets/ResNet50_mafood121_all_d_adam_f/Dataset_Mafood121_all_d.pkl'
    # Load model
    cnnmodel = loadModel(model_folder, model_reload_epoch)
    # Build dataset for preprocessing inputs
    dataset = loadDataset(dataset_filepath)
    # Predict Uncertainty
    epistemic_prediction = EpistemicPrediction(cnnmodel, dataset, monte_carlo_simulations=1)
    total_epistemic_predictions = epistemic_prediction.predict()
    # Save predictions 
    with open("output/mafood121/all_dishes_output/data_mafood121_d_adam_f_test_"+TARGET_SET+"_wod.txt", "w") as f:
        f.write(str(total_epistemic_predictions)+"\n")

    ### Cuisine classifier ###
    NCLASSES = 11
    model_folder = 'trained_models/ResNet50_DA_mafood121_c_adam_f_res50da'
    model_reload_epoch = 36
    dataset_filepath = 'datasets/ResNet50_mafood121_c_adam_f/Dataset_Mafood121_c.pkl'
    # Load model
    cnnmodel = loadModel(model_folder, model_reload_epoch)
    # Build dataset for preprocessing inputs
    dataset = loadDataset(dataset_filepath)
    # Predict Uncertainty
    epistemic_prediction = EpistemicPrediction(cnnmodel, dataset, monte_carlo_simulations=100)
    total_epistemic_predictions = epistemic_prediction.predict()
    # Save predictions 
    with open("output/mafood121/hl_output/eu_mafood121_c_adam_f_dawp_"+TARGET_SET+"_wd.txt", "w") as f:
        f.write(str(total_epistemic_predictions)+"\n")
    # Predict Uncertainty
    epistemic_prediction = EpistemicPrediction(cnnmodel, dataset, monte_carlo_simulations=1)
    total_epistemic_predictions = epistemic_prediction.predict()
    # Save predictions 
    with open("output/mafood121/hl_output/eu_mafood121_c_adam_f_dawp_"+TARGET_SET+"_wod.txt", "w") as f:
        f.write(str(total_epistemic_predictions)+"\n")

    ### Classifier for each cuisine ###
    NCLASSES = 11
    target_models = ["am", "ch", "fr", "gr", "in", "it", "ja", "me", "th", "tu", "vi"]
    epochs_models = [13,13,11,22,12,9,12,22,22,14,15]
    iter_models = [3,3,2,4,4,3,1,2,1,0,5]

    for m_cuisine, model_reload_epoch in zip(target_models, epochs_models):
        model_folder = 'trained_models/ResNet50_DA_mafood121_d_da_'+m_cuisine+'_de_'+str(iter_models[target_models.index(m_cuisine)])+'/'
        dataset_filepath = 'datasets/ResNet50_mafood121_all_d_adam_f/Dataset_Mafood121_all_d.pkl'
        # Load model
        cnnmodel = loadModel(model_folder, model_reload_epoch)
        target_datasets = [m_cuisine]
        for ds in target_datasets:
            cuisine = ds
            # Build dataset for preprocessing inputs
            dataset = loadDataset(dataset_filepath)
            epistemic_prediction = EpistemicPrediction(cnnmodel, dataset, monte_carlo_simulations=100)
            total_epistemic_predictions = epistemic_prediction.predict()
            with open("mafood121/eu_method/mcdropout/eu_mafood121_d_"+m_cuisine+"_"+cuisine+"_"+TARGET_SET+"_wd.txt", "w") as f:
                f.write(str(total_epistemic_predictions)+"\n")
            epistemic_prediction = EpistemicPrediction(cnnmodel, dataset, monte_carlo_simulations=1)
            total_epistemic_predictions = epistemic_prediction.predict()
            with open("mafood121/fg_outputs/eu_mafood121_d_"+m_cuisine+"_"+cuisine+"_"+TARGET_SET+"_wod.txt", "w") as f:
                f.write(str(total_epistemic_predictions)+"\n")
