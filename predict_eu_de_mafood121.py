# -*- coding: utf-8 -*-

from keras.callbacks import Callback
import numpy as np

from keras.models import Model
from keras.layers import Input, RepeatVector, Flatten,Reshape
from keras.engine.topology import Layer
from keras.layers.wrappers import TimeDistributed
from keras import backend as K
from keras.optimizers import Adam
import copy
from keras_wrapper.cnn_model import loadModel
from keras_wrapper.dataset import loadDataset
from keras.models import load_model, model_from_json

import tensorflow as tf
import scipy.stats

if __name__=="__main__":

    NCLASSES = 11  # 101, 256
    GPU_ID = '0'
    TARGET_SET = 'test' # test
    GPU_MEMORY_FRACTION = 0.5

    # Configure GPU memory usage for TF
    if 'tensorflow' == K.backend():
        import tensorflow as tf
        from keras.backend.tensorflow_backend import set_session

        # GPU
        config = tf.ConfigProto()
        config.gpu_options.per_process_gpu_memory_fraction = GPU_MEMORY_FRACTION
        config.gpu_options.visible_device_list = GPU_ID
        set_session(tf.Session(config=config))


    dataset_filepath = 'datasets/ResNet50_mafood121_all_d_adam_f/Dataset_Mafood121_all_d.pkl'
    # Build dataset for preprocessing inputs
    dataset = loadDataset(dataset_filepath)

    target_models = ["am", "ch", "fr", "gr", "in", "it", "ja", "me", "th", "tu", "vi"]
    niterations = 5
    epochs_models = [
                     [3,6,13,13,24],
                     [9,3,4,7,1],
                     [4,8,4,3,18],
                     [6,12,13,11,8],
                     [12,10,16,12,7],
                     [9,16,9,5,6],
                     [12,13,24,13,5],
                     [24,23,17,12,4],
                     [21,14,18,7,16],
                     [9,5,11,11,14],
                     [9,12,8,13,10]]

    trained_models_path = "trained_models/"
    
    for tm_idx, target_model in enumerate(target_models):
        print (target_model)
        
        prob_x_iter = []
        for i in range(1, niterations+1):
            print (target_model, i)
            curr_epoch = epochs_models[tm_idx][i-1]
            model_name = "ResNet50_DA_mafood121_d_da_"+target_model+"_de_"+str(i)
            model_weights = trained_models_path+model_name+"/epoch_"+str(curr_epoch)+".h5"
            model_structure = trained_models_path+model_name+"/epoch_"+str(curr_epoch)+"_Model_Wrapper.pkl"


            # Load model
            cnnmodel = loadModel(trained_models_path+model_name, curr_epoch)
            model = cnnmodel.model

            entropies_c5_edl1 = []
            batch_size = 60
            if TARGET_SET == 'val':
                total_images = dataset.len_val
            elif TARGET_SET == 'test':
                total_images = dataset.len_test

            print (total_images)
            niter = int(np.ceil(total_images/float(batch_size)))

            print ("")

            prob_x_sample = []
            for i in range(niter):
                init_index = i*batch_size
                if i < (niter -1):
                    end_index = (i+1)*batch_size
                else:
                    end_index = total_images

                X,Y = dataset.getXY(TARGET_SET, end_index-init_index, dataAugmentation=False)
                probs = model.predict(X, batch_size=end_index-init_index)
                prob_x_sample.append(probs)
            prob_x_iter.append(np.concatenate(prob_x_sample,axis=0))
        prob_x_ds = np.array(prob_x_iter)
        avg_prob_ds = np.mean(prob_x_ds, axis=0)
        #np.save("output/mafood121/eu_method/de/prob_"+target_model+"all_csds_de.npy", avg_prob_ds)

    for target_model in target_models:
        #prob = np.load("output/mafood121/eu_method/de/prob_"+target_model+"all_csds_de.npy")
        pred_x_sample = list(np.argmax(np.array(prob), axis=-1))
        entropies_x_sample = [scipy.stats.entropy(prob[i,:]) for i in range(3177)]
        np.save("output/mafood121/eu_method/de/eu_"+target_model+"all_csds_de.npy", entropies_x_sample)
        #np.save("output/mafood121/eu_method/de/pred_"+target_model+"all_csds_de.npy", pred_x_sample)
