#from  keras import objectives
import numpy as np
from keras import backend as keras_backend
from custom_losses import loss_eq5
import tensorflow as tf

def load_parameters():
    """
        Loads the defined parameters
    """

    GPUID = "0"
    GPU_MEMORY_FRACTION = 0.9
    # Input data params
    DATA_ROOT_PATH = '/home/eduardo/Documents/datasets/food101/'

    CUISINE = "fg4"
   
    DATASET_NAME = 'food101_hl_resnet50da_fg4_edl'  # Dataset name

    IMG_FILES = {'train': 'meta_fg/annotations_fg4/train.txt',  # Images files
                 'val': 'meta_fg/annotations_fg4/test.txt',
                 'test': 'meta_fg/annotations_fg4/test.txt'
                 }

    NUM_DISHES = 20 
    NUM_CLASSES = 20 
    DISHES_PATH = 'meta_fg/annotations_fg4/classes.txt'
    LABELS_DISH_FILES = {'train': 'meta_fg/annotations_fg4/full_train_labels.txt',  # Labels files
                         'val': 'meta_fg/annotations_fg4/test_labels.txt',
                         'test': 'meta_fg/annotations_fg4/test_labels.txt'
                         }
    # Evaluation
    METRICS = [
               ['multiclass_metrics']
              ]  # Metric used for evaluating model after each epoch. Possible values: 'multiclass' (see more information in utils/evaluation.py
    

    STOP_METRIC = 'accuracy_output_0' # find the best model taking into account the f1-score of the dishes prediction task
    #STOP_METRIC = 'f1_output_0' 
    MIN_PRED_VAL = 0.5

    
    CLASSIFIER_ACTIVATION = ['relu'
                            ]  # 'softmax', 'sigmoid', etc.

    train_images_cu = {
                       "fg1":750*20, 
                       "fg2":750*20, 
                       "fg3":750*20, 
                       "fg4":750*20, 
                       "fg5":750*21
                       }
    
    global_step = keras_backend.variable(value=0)
    
    total_train_images = train_images_cu[CUISINE]
    n_batches = total_train_images // 32 # train_images // bsize
    annealing_step =40*n_batches # MAX_EPOCH * n_batches
    loss = loss_eq5(NUM_CLASSES, global_step, annealing_step)
    
    LOSS = [
             # 'categorical_crossentropy'
             loss
            ]  # 'categorical_crossentropy' (better for sparse labels), 'binary_crossentropy', etc.

    WRITE_OUTPUTS_TYPE = ['list']

    NETWORK_TYPE = 'ResNet50_DA_EDL' # 'TestModel' for testing
    if 'Inception' in NETWORK_TYPE:
        # InceptionV3
        IMG_SIZE = [342, 342, 3]
        IMG_SIZE_CROP = [299, 299, 3]
        INPUTS_IDS_MODEL = ['input_1']

        INPUTS_MAPPING = {'input_1': 0}

    elif NETWORK_TYPE == 'VGG16' or NETWORK_TYPE == 'TestModel' or 'ResNet50' in NETWORK_TYPE or NETWORK_TYPE == 'Arch_D':

        IMG_SIZE = [256, 256, 3] # resize applied to the images
        IMG_SIZE_CROP = [224, 224, 3] # input size of the network (images will be cropped if DATA_AUGMENTATION==True)
        INPUTS_IDS_MODEL = ['image']  # Corresponding inputs of the built model

        INPUTS_MAPPING = {'image': 0}

    MODEL_RELOAD_PATH = "trained_models/ResNet50_food101_all_d_adam_f_2"
    MODEL_RELOAD_BEST_EPOCH = 34

    # Dataset parameters
    INPUTS_IDS_DATASET = ['image']  # Corresponding inputs of the dataset
    OUTPUTS_NLABELS = [NUM_CLASSES]
    NCLASSES = OUTPUTS_NLABELS
    OUTPUTS_TYPES = ['categorical']
    OUTPUTS_IDS_DATASET = [ 'dish_'+CUISINE]  # Corresponding outputs of the dataset
    OUTPUTS_IDS_MODEL = ['dish_'+CUISINE]  # Corresponding outputs of the built model
    OUTPUTS_MAPPING = { 'dish_'+CUISINE: 0 }

    MEAN_IMAGE = [104.0067, 116.6690, 122.6795] # image mean on the RGB channels of the training data


    # Image pre-processingparameters
    NORMALIZE_IMAGES = False
    MEAN_SUBSTRACTION = True
    DATA_AUGMENTATION = True  # only applied on training set

    # Evaluation params
    EVAL_ON_SETS = ['val', 'test']  # Possible values: 'train', 'val' and 'test'
    START_EVAL_ON_EPOCH = 1  # First epoch where the model will be evaluated

    # Optimizer parameters (see model.compile() function)
    OPTIMIZER = 'adam'
    LR_DECAY = 40  # number of minimum number of epochs before the next LR decay (set to None to disable)
    LR_GAMMA = 0.5  # multiplier used for decreasing the LR  
    LR = 0.00001  # general LR (0.001 recommended for adam optimizer)
    PRE_TRAINED_LR_MULTIPLIER = 0.001  # LR multiplier for pre-trained network (LR x PRE_TRAINED_LR_MULTIPLIER)
    NEW_LAST_LR_MULTIPLIER = 1.0  # LR multiplier for the newly added layers (LR x NEW_LAST_LR_MULTIPLIER)

    # Training parameters
    MAX_EPOCH = 32  # Stop when computed this number of epochs
    PATIENCE = 32   # number of epoch we will wait to possibly obtain a higher accuracy
    BATCH_SIZE = 32
    PARALLEL_LOADERS = 1  # parallel data batch loaders
    EPOCHS_FOR_SAVE = 40   # number of epochs between model saves
    WRITE_VALID_SAMPLES = True  # Write valid samples in file

    # Model parameters
    MODEL_TYPE = NETWORK_TYPE

    # Results plot and models storing parameters
    EXTRA_NAME = 'food101_d_da_edl_'+CUISINE # custom name assigned to the model
    MODEL_NAME = MODEL_TYPE+'_'+EXTRA_NAME
    
    REUSE_MODEL_NAME = None # 'trained_models/Inception_inception_recipes_v2' # None default
    LAST_LAYER = 'flatten_1'  # 'flatten' #(InceptionV3)
    REUSE_MODEL_RELOAD = 0

    VERBOSE = 1  # Verbosity
    REBUILD_DATASET = True  # build again (True) or use stored instance (False)
    MODE = 'training'  # 'training' or 'predict' (if 'predict' then RELOAD must be greater than 0 and EVAL_ON_SETS will be used)

    RELOAD = 0  # If 0 start training from scratch, otherwise the model saved on epoch 'RELOAD' will be used
    STORE_PATH = 'trained_models/' + MODEL_NAME  # models and evaluation results
    
    STORE_PATH_DS = 'datasets/'+ MODEL_NAME
    # ============================================
    parameters = locals().copy()
    return parameters
