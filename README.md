## Requirements/Dependencies

1. Python 2.7.16
2. Tensorflow 1.14.0
3. Keras 2.2.4
4. Multimodal Keras Wrapper 2.1.6 ([link](https://github.com/MarcBS/multimodal_keras_wrapper/tree/v2.1.6))
5. Datasets: MAFood-121 ([link](https://drive.google.com/uc?id=1lr3b7cPl_yoBK1N8thDJMYpPNCxDZFPD&export=download)) and Food101 ([link](http://data.vision.ee.ethz.ch/cvl/food-101.tar.gz))
---

We prepared a script that loads the model outputs from the flat classifier, LCPN models, and uncertainty estimation methods, and from them generates the statistics for each classification method evaluated.

All data is organized inside the folder "paper\_results", where "all\_dishes\_output" corresponds to the flat classifier output, "hl\_output" to the first level local classifier output, "fg\_outputs" to the second level local classifiers outputs and eu\_method to the uncertainty estimated for each method used.

run: python eval\_method\_[mafood121].py

---

## Experiment procedure

The experiments consist of three parts: 

1. Training of the CNN models for the classifiers used in the LCPN and flat classification approaches.
2. Training of the CNN models required to estimate epistemic uncertainty using the methods: Deep Ensemble (DE), Evidential Deep Learning (EDL) and MCdropout.
3. Final prediction generation using the proposed hybrid classifier approach, which decides whether the prediction is given by the local classifiers or by the flat classifier from the criteria proposed based on the uncertainty analysis.

Please note that our proposal is independent of any CNN architecture, training procedure or hyperparameters chosen for training. However, with the aims to replicate our experiments we recommend that you follow the following steps:

1. Train a flat classifier. (./train\_[mafood121]\_all\_d.sh)
2. Add the path and best epoch of the trained model in 1 to config\_[mafood121]\_c.py. 
3. Train a CNN model for the Local Parent Classifier (LPC), initializing the weights with the model trained in 1. (./train\_[mafood121]\_c.sh)
4. Add the path and best epoch of the trained model in 1 to config\_[mafood121]\_sc\_[am].py and config\_[mafood121]\_sc\_edl\_[am].py. 
5. Train five CNN models for DE (./train\_[mafood121]\_sc\_de.sh) and one for EDL (./train\_[mafood121]\_sc\_edl.sh) for each Local Child Classifier (LCC), initializing the weights with the model trained in 1.
6. Select the best models, according to the results obtained in the validation set, to be used both to estimate the uncertainty using MCdropout and for the classification corresponding to the LCC.
7. Estimate the epistemic uncertainty using each method. (python predict\_eu\_de\_[mafood121].py, python predict\_eu\_edl\_[mafood121].py and python predict\_eu\_mc\_[mafood121].py)
8. Modify the output directory path in eval\_method\_[mafood121].py, and run it to evaluate LCPN, FLAT and the proposed Hybrid Classifier. (python eval\_method\_[mafood121].py) 

Specifically for Food101, step 3 corresponds to completely retraining a pre-trained InceptionResNetv2 network on ImageNet instead of adjusting the trained ResNet50 model to 1. For the remaining models, we use ResNet50 in the same way as with the MAFood121 dataset.

Keep in mind, as we can expect, the better the local classifier of the LCPN approach works, the better results we can get with the proposed hybrid classifier.
