from keras.engine import Input
from keras.layers import concatenate as merge
from keras.layers.core import Dropout, Dense, Flatten, Activation
from keras.models import model_from_json, Model
from keras.applications.resnet50 import ResNet50
from keras.applications.inception_resnet_v2 import InceptionResNetV2

from keras_wrapper.cnn_model import Model_Wrapper
from keras_wrapper.cnn_model import loadModel
import numpy as np
import os
import logging
import shutil
import time
from keras.regularizers import l1, l2


class CNN_Model(Model_Wrapper):
    
    def __init__(self, params, type, verbose=1, structure_path=None, weights_path=None,
                 model_name=None, store_path=None, seq_to_functional=False):
        """
            CNN_Model object constructor.
            
            :param params: all hyperparameters of the model.
            :param type: network name type (corresponds to any method defined in the section 'MODELS' of this class). Only valid if 'structure_path' == None.
            :param verbose: set to 0 if you don't want the model to output informative messages
            :param structure_path: path to a Keras' model json file. If we speficy this parameter then 'type' will be only an informative parameter.
            :param weights_path: path to the pre-trained weights file (if None, then it will be randomly initialized)
            :param model_name: optional name given to the network (if None, then it will be assigned to current time as its name)
            :param store_path: path to the folder where the temporal model packups will be stored
            :param seq_to_functional: defines if we are loading a set of weights from a Sequential model to a FunctionalAPI model (only applicable if weights_path is not None)

        """
        super(self.__class__, self).__init__( model_name=model_name,
                                             silence=verbose == 0, models_path=store_path, inheritance=True)
        
        self.__toprint = ['_model_type', 'name', 'model_path', 'verbose']
        
        self.verbose = verbose
        self._model_type = type
        self.params = params

        # Sets the model name and prepares the folders for storing the models
        self.setName(model_name, store_path)

        # Prepare model
        if structure_path:
            # Load a .json model
            if self.verbose > 0:
                logging.info("<<< Loading model structure from file "+ structure_path +" >>>")
            self.model = model_from_json(open(structure_path).read())
        else:
            # Build model from scratch
            if hasattr(self, type):
                if self.verbose > 0:
                    logging.info("<<< Building "+ type +" CNN_Model >>>")
                eval('self.'+type+'(params)')
            else:
                raise Exception('CNN_Model type "'+ type +'" is not implemented.')
        
        # Load weights from file
        if weights_path:
            if self.verbose > 0:
                logging.info("<<< Loading weights from file "+ weights_path +" >>>")
            self.model.load_weights(weights_path, seq_to_functional=seq_to_functional)
        
        # Print information of self
        if verbose > 0:
            print (str(self))
            self.model.summary()

        self.setOptimizer()
        
        
    def setOptimizer(self, metrics=['acc']):

        """
            Sets a new optimizer for the model.
        """

        super(self.__class__, self).setOptimizer(lr=self.params['LR'],
                                                 loss=self.params['LOSS'],
                                                 optimizer=self.params['OPTIMIZER'],
                                                 loss_weights=self.params.get('LOSS_WIGHTS', None),
                                                 sample_weight_mode='temporal' if self.params.get('SAMPLE_WEIGHTS', False) else None)

    
    def setName(self, model_name, store_path=None, clear_dirs=True):
        """
            Changes the name (identifier) of the model instance.
        """
        if model_name is None:
            self.name = time.strftime("%Y-%m-%d") + '_' + time.strftime("%X")
            create_dirs = False
        else:
            self.name = model_name
            create_dirs = True

        if store_path is None:
            self.model_path = 'Models/' + self.name
        else:
            self.model_path = store_path


        # Remove directories if existed
        if clear_dirs:
            if os.path.isdir(self.model_path):
                shutil.rmtree(self.model_path)

        # Create new ones
        if create_dirs:
            if not os.path.isdir(self.model_path):
                os.makedirs(self.model_path)

    def decode_predictions(self, preds, temperature, index2word, sampling_type=None, verbose=0):
        """
        Decodes predictions

        In:
            preds - predictions codified as the output of a softmax activiation function
            temperature - temperature for sampling (not used for this model)
            index2word - mapping from word indices into word characters
            sampling_type - sampling type (not used for this model)
            verbose - verbosity level, by default 0

        Out:
            Answer predictions (list of answers)
        """
        labels_pred = np.where(np.array(preds) > 0.5, 1, 0)
        labels_pred = [[index2word[e] for e in np.where(labels_pred[i] == 1)[0]] for i in range(labels_pred.shape[0])]
        return labels_pred
    
    # ------------------------------------------------------- #
    #       VISUALIZATION
    #           Methods for visualization
    # ------------------------------------------------------- #
    
    def __str__(self):
        """
            Plot basic model information.
        """
        obj_str = '-----------------------------------------------------------------------------------\n'
        class_name = self.__class__.__name__
        obj_str += '\t\t'+class_name +' instance\n'
        obj_str += '-----------------------------------------------------------------------------------\n'
        
        # Print pickled attributes
        for att in self.__toprint:
            obj_str += att + ': ' + str(self.__dict__[att])
            obj_str += '\n'
            
        obj_str += '\n'
        obj_str += 'MODEL PARAMETERS:\n'
        obj_str += str(self.params)
        obj_str += '\n'
            
        obj_str += '-----------------------------------------------------------------------------------'
        
        return obj_str
    
    
    # ------------------------------------------------------- #
    #       PREDEFINED MODELS
    # ------------------------------------------------------- #

    def ResNet50(self, params):

        self.ids_inputs = params["INPUTS_IDS_MODEL"]
        self.ids_outputs = params["OUTPUTS_IDS_MODEL"]

        activation_type = params['CLASSIFIER_ACTIVATION']

        input_shape = params['IMG_SIZE_CROP']
        image = Input(name=self.ids_inputs[0], shape=input_shape)

        ##################################################
        # Load Inception model pre-trained on ImageNet
        base_model = ResNet50(weights='imagenet', input_tensor=image)

        # Recover last layer kept from original model: 'fc2'
        x = base_model.get_layer('avg_pool').output   

        x_model = Dense(2048, activation='relu',name='fc1_Dense')(x)
        x_model = Dropout(0.5, name='dropout_1')(x_model)
        nOutput = params['NUM_CLASSES']

        x = Dense(nOutput, activation=activation_type[0], name=self.ids_outputs[0])(x_model)
        self.model = Model(input=image, output=x)

    def InceptionResNetV2(self, params):

        self.ids_inputs = params["INPUTS_IDS_MODEL"]
        self.ids_outputs = params["OUTPUTS_IDS_MODEL"]

        activation_type = params['CLASSIFIER_ACTIVATION']

        input_shape = params['IMG_SIZE_CROP']
        image = Input(name=self.ids_inputs[0], shape=input_shape)

        ##################################################
        # Load Inception model pre-trained on ImageNet
        base_model = InceptionResNetV2(weights='imagenet', input_tensor=image)

        # Recover last layer kept from original model: 'fc2'
        x = base_model.get_layer('avg_pool').output   

        x_model = Dense(2048, activation='relu',name='fc1_Dense')(x)
        x_model = Dropout(0.5, name='dropout_1')(x_model)
        nOutput = params['NUM_CLASSES']

        x = Dense(nOutput, activation=activation_type[0], name=self.ids_outputs[0])(x_model)

        self.model = Model(input=image, output=x)

    def ResNet50_DA(self, params):

        self.ids_inputs = params["INPUTS_IDS_MODEL"]
        self.ids_outputs = params["OUTPUTS_IDS_MODEL"]

        activation_type = params['CLASSIFIER_ACTIVATION']

        input_shape = params['IMG_SIZE_CROP']
        image = Input(name=self.ids_inputs[0], shape=input_shape)

        ##################################################
        # Load Inception model pre-trained on ImageNet
        model_folder = params['MODEL_RELOAD_PATH']
        model_reload_epoch = params['MODEL_RELOAD_BEST_EPOCH']

        # Load model
        base_model = loadModel(model_folder, model_reload_epoch).model

        for layer in base_model.layers:
            layer.trainable = False

        # Recover last layer kept from original model: 'fc2'
        x = base_model.get_layer('avg_pool').output   

        x_model = Dense(2048, activation='relu',name='fc1_Dense')(x)
        x_model = Dropout(0.5, name='dropout_2')(x_model)
        nOutput = params['NUM_CLASSES']

        x = Dense(nOutput, activation=activation_type[0], name=self.ids_outputs[0])(x_model)
        self.model = Model(input=base_model.input, output=x)


    def ResNet50_DA_EDL(self, params):

        self.ids_inputs = params["INPUTS_IDS_MODEL"]
        self.ids_outputs = params["OUTPUTS_IDS_MODEL"]

        activation_type = params['CLASSIFIER_ACTIVATION']

        input_shape = params['IMG_SIZE_CROP']
        image = Input(name=self.ids_inputs[0], shape=input_shape)

        ##################################################
        # Load Inception model pre-trained on ImageNet
        model_folder = params['MODEL_RELOAD_PATH']
        model_reload_epoch = params['MODEL_RELOAD_BEST_EPOCH']

        # Load model
        base_model = loadModel(model_folder, model_reload_epoch).model

        for layer in base_model.layers:
            layer.trainable = False

        # Recover last layer kept from original model: 'fc2'
        x = base_model.get_layer('avg_pool').output   

        x_model = Dense(2048, activation='relu',name='fc1_Dense')(x) #, kernel_regularizer=l2(0.005))(x)
        x_model = Dropout(0.5, name='dropout_1')(x_model)

        nOutput = params['NUM_CLASSES']
        # Activation RELU required for EDL
        x = Dense(nOutput, activation="relu", name=self.ids_outputs[0])(x_model) #, kernel_regularizer=l2(0.005))(x_model)
        self.model = Model(input=base_model.input, output=x)

    def ResNet50_EDL(self, params):

        self.ids_inputs = params["INPUTS_IDS_MODEL"]
        self.ids_outputs = params["OUTPUTS_IDS_MODEL"]

        activation_type = params['CLASSIFIER_ACTIVATION']

        input_shape = params['IMG_SIZE_CROP']
        image = Input(name=self.ids_inputs[0], shape=input_shape)

        ##################################################
        # Load Inception model pre-trained on ImageNet
        base_model = ResNet50(weights='imagenet', input_tensor=image)

        x = base_model.get_layer('avg_pool').output   

        x_model = Dense(2048, activation='relu',name='fc1_Dense')(x)
        x_model = Dropout(0.5, name='dropout_1')(x_model)

        nOutput = params['NUM_CLASSES']
        # Activation RELU required for EDL
        x = Dense(nOutput, activation="relu", name=self.ids_outputs[0])(x_model)
        self.model = Model(input=base_model.input, output=x)

