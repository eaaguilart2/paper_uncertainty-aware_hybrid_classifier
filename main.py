import logging
import numpy as np
from timeit import default_timer as timer
import copy
import sys

from keras_wrapper.cnn_model import saveModel, loadModel
from keras_wrapper.extra import evaluation
from keras_wrapper.extra.callbacks import EvalPerformance
from keras_wrapper.extra.read_write import *
from keras_wrapper.utils import decode_multilabel
from keras.callbacks import Callback
from data_engine.prepare_data import build_dataset
from model import CNN_Model
from keras import backend as K

logging.basicConfig(level=logging.DEBUG, format='[%(asctime)s] %(message)s', datefmt='%d/%m/%Y %H:%M:%S')
logger = logging.getLogger(__name__)      

def train_model(params):
    """
        Main function
    """

    if(params['RELOAD'] > 0):
        logging.info('Resuming training.')
        
    ########### Load data
    dataset = build_dataset(params)
    ###########

    ########### Build model
    if params['REUSE_MODEL_NAME'] is not None and params['REUSE_MODEL_RELOAD'] > 0:
        multitask_model = loadModel(params['REUSE_MODEL_NAME'], params['REUSE_MODEL_RELOAD'])
        multitask_model.setName(model_name=params['MODEL_NAME'], store_path=params['STORE_PATH'])
        multitask_model.changeClassifier(params, last_layer=params['LAST_LAYER'])
        multitask_model.updateLogger(force=True)
        
    elif(params['RELOAD'] == 0): # build new model
        multitask_model = CNN_Model(params, type=params['MODEL_TYPE'], verbose=params['VERBOSE'],
                                model_name=params['MODEL_NAME'], store_path=params['STORE_PATH'])
       
        # Define the inputs and outputs mapping from our Dataset instance to our model
        multitask_model.setInputsMapping(params['INPUTS_MAPPING'])
        multitask_model.setOutputsMapping(params['OUTPUTS_MAPPING'])

    else: # resume from previously trained model
        multitask_model = loadModel(params['STORE_PATH'], params['RELOAD'])
    # Update optimizer either if we are loading or building a model
    multitask_model.params = params
    multitask_model.setOptimizer()
    ###########

    
    ########### Callbacks
    callbacks = buildCallbacks(params, multitask_model, dataset)
    ###########
    

    ########### Training
    total_start_time = timer()

    logger.debug('Starting training!')
    training_params = {'n_epochs': params['MAX_EPOCH'], 'batch_size': params['BATCH_SIZE'],
                       'lr_decay': params['LR_DECAY'], 'lr_gamma': params['LR_GAMMA'],
                       'initial_lr': params['LR'],
                       'start_reduction_on_epoch': params['LR_DECAY']-1,
                       'shuffle':True,
                       'epochs_for_save': params['EPOCHS_FOR_SAVE'], 'verbose': params['VERBOSE'],
                       'n_parallel_loaders': params['PARALLEL_LOADERS'],
                       'extra_callbacks': callbacks, 'reload_epoch': params['RELOAD'], 'epoch_offset': params['RELOAD'],
                       'data_augmentation': params['DATA_AUGMENTATION'],
                       'patience': params['PATIENCE'], 'metric_check': params['STOP_METRIC']
                       }
    multitask_model.trainNet(dataset, training_params)
    
    total_end_time = timer()
    time_difference = total_end_time - total_start_time
    logging.info('Total time spent {0:.2f}s = {1:.2f}m'.format(time_difference, time_difference / 60.0))
    ###########
    

def apply_model(params):
    """
        Function for using a previously trained model for predicting.
    """
    
    
    ########### Load data
    dataset = build_dataset(params)
    ###########
    
    
    ########### Load model
    multitask_model = loadModel(params['STORE_PATH'], params['RELOAD'])
    multitask_model.setOptimizer()
    ###########
    

    ########### Apply evaluation
    callbacks = buildCallbacks(params, multitask_model, dataset)
    callbacks[0].evaluate(params['RELOAD'], 'epoch')
    ###########


def buildCallbacks(params, model, dataset):
    """
        Builds the selected set of callbacks run during the training of the model
    """
    callbacks = []

    if params['METRICS']:
        # Evaluate training
        extra_vars = dict()
        extra_vars['n_parallel_loaders'] = params['PARALLEL_LOADERS']

        for i in range(len(params['OUTPUTS_IDS_DATASET'])):
            extra_vars[i] = dict()
        for s in params['EVAL_ON_SETS']:
            extra_vars[s] = dict()
            # dishes
            extra_vars[0]['n_classes'] = params['NUM_DISHES']
            extra_vars[0][s] = dict()
            #exec("extra_vars[1][s]['references'] = dataset.Y_" + s + "[params['OUTPUTS_IDS_DATASET'][1]]")

        # Vocabularies for outputs of type binary
        vocab = [
                 ''
                ]
        
        
        vocab_x = [
                   None
                  ]

        callback_metric = EvalPerformance(model, dataset,
                                           gt_id=params['OUTPUTS_IDS_DATASET'],
                                           gt_pos=range(len(params['OUTPUTS_TYPES'])),
                                           metric_name=params['METRICS'],
                                           set_name=params['EVAL_ON_SETS'],
                                           batch_size=params['BATCH_SIZE'],
                                           output_types=params['OUTPUTS_TYPES'],
                                           min_pred_multilabel=params['MIN_PRED_VAL'],
                                           index2word_y=vocab, # text info
                                           index2word_x=vocab_x,
                                           save_path=model.model_path,
                                           reload_epoch=params['RELOAD'],
                                           start_eval_on_epoch=params['START_EVAL_ON_EPOCH'],
                                           write_samples=params['WRITE_VALID_SAMPLES'],
                                           write_type=params['WRITE_OUTPUTS_TYPE'],
                                           extra_vars=extra_vars,
                                           verbose=params['VERBOSE'],
                                           do_plot=False)
        callbacks.append(callback_metric)

    return callbacks

def make_classes_file(params):
    dataset = build_dataset(params)
    dataset.extra_variables['word2idx_ing']
    logging.info(dataset.Y_train['ingredients'][0])
    logging.info(dataset.Y_train['dish'][0])
    logging.info(dataset.Y_train['style'][0])
    logging.info(dataset.Y_train['families'][0])
    logging.info(dataset.Y_train['combines'][0])    
    logging.info(dataset.loadBinary(dataset.Y_train['combines'], 'combines')[0])
    logging.info(dataset.loadBinary([['0'],['0','1'],['5','8']], 'combines'))

if __name__ == "__main__":
    # Use 'config_file' command line parameter for changing the name of the config file used
    cf = 'config'
    for arg in sys.argv[1:]:
        k, v = arg.split('=')
        if k == 'config_file':
            cf = v
    cf = __import__(cf)
    params = cf.load_parameters()

     # Configure GPU memory usage for TF
    if 'tensorflow' == K.backend():
        import tensorflow as tf
        from keras.backend.tensorflow_backend import set_session

    # Reset session (memory) before starting next network training
    if 'tensorflow' == K.backend():
        config = tf.ConfigProto()
        config.gpu_options.visible_device_list = params['GPUID']
        config.gpu_options.allow_growth = True
        set_session(tf.Session(config=config))

    if(params['MODE'] == 'training'):
        logging.info('Running training.')
        train_model(params)

    logging.info('Done!')
