import numpy as np
import os

if __name__ == "__main__":

    METHODS = ["Proposed Method", "LCPN", "FLAT"]
    CRITERIA = [
                "np.argmin(eu_x_sample_dishes[:,j], axis=-1)==predswod[j]",
                "np.argmin(eu_x_sample_dishes[:,j], axis=-1)==preds[j]",
                "np.argmin(eu_x_sample_dishes[:,j], axis=-1)==predswod[j] or np.argmin(eu_x_sample_dishes[:,j], axis=-1)==preds[j]", 
                "np.argmin(eu_x_sample_dishes[:,j], axis=-1)==predswod[j] and np.argmin(eu_x_sample_dishes[:,j], axis=-1)==preds[j]"
                ]
    TARGET_SET = 'test'
    EU_METHOD = "DE"  # DE, EDL or MCdropout
    OUTPUT_FOLDER = "paper_results/"
    IMAGES_PATH = OUTPUT_FOLDER+"food101/meta/test_images_food101.npy"
    # Load Images
    if not os.path.isfile(IMAGES_PATH):
        dataset_filepath = 'datasets/ResNet50_food101_all_d_adam_f/Dataset_food101_all_d.pkl'
        TARGET_SET = 'test'
        dataset = loadDataset(dataset_filepath)
        images = np.array(getattr(dataset, 'X_' + TARGET_SET)["image"])
        np.save(IMAGES_PATH, images)
    else:
        images = np.load(IMAGES_PATH)

    data_fname = OUTPUT_FOLDER+"food101/hl_output/data_food101_foodgroups_"+TARGET_SET+"_wd.txt"    
    with open(data_fname, "r") as f:
        data_wd = eval(f.readlines()[0])

    datawod_fname = OUTPUT_FOLDER+"food101/hl_output/data_food101_foodgroups_"+TARGET_SET+"_wod.txt"
    with open(datawod_fname,"r") as f2:
        data_wod = eval(f2.readlines()[0])

    eu_x_sample, mean_acc, preds, gts, probs = data_wd
    eu_x_sample = np.array(eu_x_sample[0])
    gts = np.array(gts[0])
    preds = np.array(preds[0])

    _, meanwod_acc, predswod, gtswod, probswod = data_wod
    gtswod = np.array(gtswod[0])
    predswod = np.array(predswod[0])

    targets_datasets = ["fg1", "fg2", "fg3", "fg4", "fg5"] 
    eu_x_sample_dishes = []
    models_results_wd = []
    models_results_wod = []
    classes = []
    
    for ds in targets_datasets:

        if EU_METHOD == "EDL":
            eu_fname = OUTPUT_FOLDER+"food101/eu_method/edl/eu_"+ds+"all_csds_edl.npy"
            eu_x_sample_dishes.append(np.load(eu_fname, allow_pickle=True))
        elif EU_METHOD == "DE":
            eu_fname = OUTPUT_FOLDER+"food101/eu_method/de/eu_"+ds+"all_de.npy"
            eu_x_sample_dishes.append(np.load(eu_fname, allow_pickle=True))
        elif EU_METHOD == "MCdropout":
            datawd_fname = OUTPUT_FOLDER+"food101/eu_method/mcdropout/data_food101_d_"+ds+"_"+ds+"_"+TARGET_SET+"_wd.txt"
            with open(datawd_fname,"r") as f3:
                data_wd_wd = eval(f3.readlines()[0])
                models_results_wd.append(data_wd_wd)

        datawod_fname = OUTPUT_FOLDER+"food101/fg_outputs/data_food101_d_"+ds+"_"+ds+"_"+TARGET_SET+"_wod.txt"
        with open(datawod_fname,"r") as f3:
            data_wod = eval(f3.readlines()[0])
            models_results_wod.append(data_wod)

        classes_c_fname  = OUTPUT_FOLDER+"food101/meta/annotations_"+ds+"/classes.txt"
        with open(classes_c_fname,"r") as f4:
            classes_c = []
            for line in f4:
                classes_c.append("d_"+line.strip())
            classes.append(classes_c)

    if EU_METHOD == "MCdropout":
        eu_x_sample_dishes = [result[0][0] for result in models_results_wd]
    eu_x_sample_dishes = np.array(eu_x_sample_dishes)
    # Load results from general classifier (gc)
    datafd_fname = OUTPUT_FOLDER+"food101/all_dishes_output/data_food101_all_test_wod.txt"
    with open(datafd_fname, "r") as f5:
        data_wod = eval(f5.readlines()[0])
        _, _, preds_gc, _, _ = data_wod
        preds_gc = np.array(preds_gc[0])

    # Load labels gc
    classes_gc = []
    classes_gc_fname = OUTPUT_FOLDER+"food101/meta/classes.txt"
    with open(classes_gc_fname,"r") as f5:
        for line in f5:
            classes_gc.append("d_"+line.strip())

    idx_to_unc = dict()
    print "LCC", "LPC_rec", "LCC_acc_w_LPC_errors", "LPC_errors", "total_image_criteria", "total_image", "acc_FLAT_part", "overall_acc"
    for method in METHODS:
        print method
        if method == "FLAT":
            overall_acc = 0.0
            for idx, c in enumerate(targets_datasets):
                idxs_c = np.argwhere(np.array(gts)==idx)
                count = 0
                count_2 = 0
                acc = 0
                count_gc = 0
                acc_gc = 0
                for j in np.hstack(idxs_c):
                    gt = classes_gc.index("d_"+images[j].split("/")[0])
                    if preds_gc[j]==gt:
                        acc_gc = acc_gc + 1
                        overall_acc = overall_acc + 1
                    count_gc = count_gc + 1
                acc1 = 0 if count == 0 else acc/float(count)
                acc2 = 0 if count_2 == 0 else acc/float(count_2)
                acc3 = 0 if count_gc == 0 else acc_gc/float(count_gc)

                print c,np.sum(gtswod[idxs_c] == predswod[idxs_c])/float(len(idxs_c)), acc1, count-count_2, str(count) , str(len(idxs_c)), acc3, (acc+acc_gc)/float(count + count_gc)
            print "overall accuracy: ", round(overall_acc/len(images),4)
        elif method == "LCPN":
            overall_acc = 0.0
            for idx, c in enumerate(targets_datasets):
                idxs_c = np.argwhere(np.array(gts)==idx)
                count = 0
                count_2 = 0
                acc = 0
                count_gc = 0
                acc_gc = 0
                for j in np.hstack(idxs_c):
                    if gtswod[j]==predswod[j]:
                        _, meanwod_acc_c, predswod_c, gtswod_c, probswod_c = models_results_wod[gtswod[j]]
                        gt = classes[idx].index("d_"+images[j].split("/")[0])
                        predswod_c = np.array(predswod_c[0])
                        if gt == predswod_c[j]:
                            acc = acc + 1
                            overall_acc = overall_acc + 1
                        else:
                            pass
                        count_2 = count_2 + 1
                    count = count + 1
                acc1 = 0 if count == 0 else acc/float(count)
                acc2 = 0 if count_2 == 0 else acc/float(count_2)
                acc3 = 0 if count_gc == 0 else acc_gc/float(count_gc)
                print c,np.sum(gtswod[idxs_c] == predswod[idxs_c])/float(len(idxs_c)), acc1, count-count_2, str(count) , str(len(idxs_c)), acc3, (acc+acc_gc)/float(count + count_gc)
            print "overall accuracy: ", round(overall_acc/len(images),4)
        elif method == "Proposed Method":
            for criterion in CRITERIA:
                print criterion
                overall_acc = 0.0
                for idx, c in enumerate(targets_datasets):
                    idxs_c = np.argwhere(np.array(gts)==idx)
                    count = 0
                    count_2 = 0
                    acc = 0
                    count_gc = 0
                    acc_gc = 0
                    for j in np.hstack(idxs_c):
                        if eval(criterion): 
                            if gtswod[j]==predswod[j]:
                                _, meanwod_acc_c, predswod_c, gtswod_c, probswod_c = models_results_wod[gtswod[j]]
                                gt = classes[idx].index("d_"+images[j].split("/")[0])
                                predswod_c = np.array(predswod_c[0])
                                if gt == predswod_c[j]:
                                    acc = acc + 1
                                    overall_acc = overall_acc + 1
                                else:
                                    pass
                                count_2 = count_2 + 1
                            count = count + 1
                        else:
                            gt = classes_gc.index("d_"+images[j].split("/")[0])
                            if preds_gc[j]==gt:
                                acc_gc = acc_gc + 1
                                overall_acc = overall_acc + 1
                            else:
                                pass
                            count_gc = count_gc + 1
                    acc1 = 0 if count == 0 else acc/float(count)
                    acc2 = 0 if count_2 == 0 else acc/float(count_2)
                    acc3 = 0 if count_gc == 0 else acc_gc/float(count_gc)
                    print c,np.sum(gtswod[idxs_c] == predswod[idxs_c])/float(len(idxs_c)), acc1, count-count_2, str(count) , str(len(idxs_c)), acc3, (acc+acc_gc)/float(count + count_gc)
                print "overall accuracy: ", round(overall_acc/len(images),4)
        print ""
