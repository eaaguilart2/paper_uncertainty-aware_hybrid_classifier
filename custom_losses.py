# -*- coding: utf-8 -*-
from keras import backend as K
import numpy as np
import tensorflow as tf

########## START EDL ############
# Utility Function

#### Logit to evidence converters - activation functions (they have to produce non-negative outputs for the uncertaintyuncertainity process)

def relu_evidence(logits):
    return tf.nn.relu(logits)

def exp_evidence(logits): 
    return tf.exp(logits/1000)
  
def relu6_evidence(logits):
    return tf.nn.relu6(logits)
  
def softsign_evidence(logits):
    return tf.nn.softsign(logits)

#### KL Divergence calculator

def KL(alpha, K):
    beta=tf.constant(np.ones((1,K)),dtype=tf.float32)
    S_alpha = tf.reduce_sum(alpha,axis=1,keepdims=True)
    
    KL = tf.reduce_sum((alpha - beta)*(tf.digamma(alpha)-tf.digamma(S_alpha)),axis=1,keepdims=True) + \
         tf.lgamma(S_alpha) - tf.reduce_sum(tf.lgamma(alpha),axis=1,keepdims=True) + \
         tf.reduce_sum(tf.lgamma(beta),axis=1,keepdims=True) - tf.lgamma(tf.reduce_sum(beta,axis=1,keepdims=True))
    return KL


##### Loss functions (there are three different one defined in the papaer)

def loss_eq5(K, global_step, annealing_step):
    def loss(y_true, y_pred):
        p = y_true
        alpha = y_pred + 1
        S = tf.reduce_sum(alpha, axis=1, keepdims=True)
        loglikelihood = tf.reduce_sum((p-(alpha/S))**2, axis=1, keepdims=True) + tf.reduce_sum(alpha*(S-alpha)/(S*S*(S+1)), axis=1, keepdims=True)
        KL_reg =  tf.minimum(1.0, tf.cast(global_step,tf.float32)/tf.cast(annealing_step, tf.float32)) * KL((alpha - 1)*(1-p) + 1 , K)
        return tf.reduce_mean(loglikelihood + KL_reg) #* tf.cast(0,tf.float32) + tf.minimum(1.0, tf.cast(global_step/annealing_step, tf.float32))
    return loss

########## END EDL ############
