# -*- coding: utf-8 -*-

from keras.callbacks import Callback
import numpy as np

from keras.models import Model
from keras.layers import Input, RepeatVector, Flatten,Reshape
from keras.engine.topology import Layer
from keras.layers.wrappers import TimeDistributed
from keras import backend as K
from keras.optimizers import Adam
import copy
from keras_wrapper.cnn_model import loadModel
from keras_wrapper.dataset import loadDataset
from keras.models import load_model, model_from_json

import tensorflow as tf
import scipy.stats

if __name__=="__main__":

    NCLASSES = 11  # 101, 256
    GPU_ID = '0'
    TARGET_SET = 'test' # test
    GPU_MEMORY_FRACTION = 0.3

    # Configure GPU memory usage for TF
    if 'tensorflow' == K.backend():
        import tensorflow as tf
        from keras.backend.tensorflow_backend import set_session
        # GPU
        config = tf.ConfigProto()
        config.gpu_options.per_process_gpu_memory_fraction = GPU_MEMORY_FRACTION
        config.gpu_options.visible_device_list = GPU_ID
        set_session(tf.Session(config=config))


    target_models = ["am", "ch", "fr", "gr", "in", "it", "ja", "me", "th", "tu", "vi"]
    epochs_models = [38 ,34 ,39 ,26 ,26 ,38 ,27 ,39 ,32 ,28 ,26 ]

    dataset_filepath = 'datasets/ResNet50_mafood121_all_d_adam_f/Dataset_Mafood121_all_d.pkl'
    # Build dataset for preprocessing inputs
    dataset = loadDataset(dataset_filepath)

    for idx, target_model in enumerate(target_models):
        target_model = target_models[idx]

        model_folder = 'trained_models/ResNet50_DA_EDL_mafood121_d_da_'+target_model
        model_reload_epoch = epochs_models[target_models.index(target_model)]
        # Load model
        with open(model_folder+"/epoch_"+str(model_reload_epoch)+"_structure.json", "r") as f:
            str_json = f.readline()
            model = model_from_json(str_json) 
        model.load_weights(model_folder+"/epoch_"+str(model_reload_epoch)+".h5")

        entropies_c5_edl1 = []
        batch_size = 60
        if TARGET_SET == 'val':
            total_images = dataset.len_val
        elif TARGET_SET == 'test':
            total_images = dataset.len_test

        niter = int(np.ceil(total_images/float(batch_size)))

        print ""

        eu_x_sample = []
        pred_x_sample = []
        for i in range(niter):
            init_index = i*batch_size
            if i < (niter -1):
                end_index = (i+1)*batch_size
            else:
                end_index = total_images

            X,Y = dataset.getXY(TARGET_SET, end_index-init_index, dataAugmentation=False)

            logits = model.predict(X, batch_size=end_index-init_index)
            evidence = logits
            alpha = evidence + 1
            u = NCLASSES / tf.reduce_sum(alpha, axis=1, keepdims=True)
            prob = alpha/tf.reduce_sum(alpha, 1, keepdims=True)
            nm_p_c5_edl1 = K.get_value(prob)
            pred_x_sample +=  list(np.argmax(np.array(nm_p_c5_edl1), axis=-1))
            entropies_c5_edl1 += [scipy.stats.entropy(nm_p_c5_edl1[i,:]) for i in range(end_index-init_index)]

        eu_x_ds = np.array(entropies_c5_edl1)
        pred_x_ds = np.array(pred_x_sample)
        np.save("output/mafood121/eu_method/edl/eu_"+target_model+"all_csds_edl.npy", eu_x_ds)
        #np.save("output/mafood121/eu_method/edl/pred_"+target_model+"all_csds_edl.npy", pred_x_ds)
