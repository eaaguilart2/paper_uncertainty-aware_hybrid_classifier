# -*- coding: utf-8 -*-

from keras.callbacks import Callback
import numpy as np

from keras.models import Model
from keras.layers import Input, RepeatVector, Flatten,Reshape
from keras.engine.topology import Layer
from keras.layers.wrappers import TimeDistributed
from keras import backend as K
from keras.optimizers import Adam
import copy
from keras_wrapper.cnn_model import loadModel
from keras_wrapper.dataset import loadDataset
from keras.models import load_model, model_from_json

import tensorflow as tf
import scipy.stats

if __name__=="__main__":

    NCLASSES = 11  # 101, 256
    GPU_ID = '0'
    TARGET_SET = 'test' # test
    GPU_MEMORY_FRACTION = 0.8

    # Configure GPU memory usage for TF
    if 'tensorflow' == K.backend():
        import tensorflow as tf
        from keras.backend.tensorflow_backend import set_session
        # GPU
        config = tf.ConfigProto()
        config.gpu_options.per_process_gpu_memory_fraction = GPU_MEMORY_FRACTION
        config.gpu_options.visible_device_list = GPU_ID
        set_session(tf.Session(config=config))


    dataset_filepath = 'datasets/ResNet50_food101_all_d_adam_f/Dataset_food101_all_d.pkl'
    # Build dataset for preprocessing inputs
    dataset = loadDataset(dataset_filepath)

    target_models = ["fg1", "fg2", "fg3", "fg4", "fg5"]
    niterations = 5
    epochs_models = [[22,12,4,15,9],
                     [13,16,12,12,7],
                     [31,8,8,3,20],
                     [2,7,15,9,11],
                     [20,15,3,14,12]
                    ]

    for tm_idx, target_model in enumerate(target_models):
        prob_x_iter = []
        for i in range(1, niterations+1):
            print (target_model, i)
            curr_epoch = epochs_models[tm_idx][i-1]
            suffix = "" if i == 1 else "_"+str(i)
            model_name = 'trained_models/ResNet50_DA_food101_fg_adam_f_res50dar_'+target_model+'_de_'+suffix
            model_weights = model_name+"/epoch_"+str(curr_epoch)+".h5"
            model_structure = model_name+"/epoch_"+str(curr_epoch)+"_Model_Wrapper.pkl"

            # Load model
            cnnmodel = loadModel(trained_models_path+model_name, curr_epoch)
            model = cnnmodel.model

            entropies_c5_edl1 = []
            batch_size = 60
            if TARGET_SET == 'val':
                total_images = dataset.len_val
            elif TARGET_SET == 'test':
                total_images = dataset.len_test

            print (total_images)
            niter = int(np.ceil(total_images/float(batch_size)))

            print ("")

            prob_x_sample = []
            for i in range(niter):
                init_index = i*batch_size
                if i < (niter -1):
                    end_index = (i+1)*batch_size
                else:
                    end_index = total_images

                X,Y = dataset.getXY(TARGET_SET, end_index-init_index, dataAugmentation=False)
                probs = model.predict(X, batch_size=end_index-init_index)
                prob_x_sample.append(probs)
            prob_x_iter.append(np.concatenate(prob_x_sample,axis=0))
        prob_x_ds = np.array(prob_x_iter)
        avg_prob_ds = np.mean(prob_x_ds, axis=0)
        #np.save("output/food101/eu_method/de/prob_"+target_model+"all_de.npy", avg_prob_ds)

    for target_model in target_models:
        #prob = np.load("prob_"+target_model+"all_de.npy")
        pred_x_sample = list(np.argmax(np.array(prob), axis=-1))
        entropies_x_sample = [scipy.stats.entropy(prob[i,:]) for i in range(np.shape(prob)[0])]
        np.save("output/food101/eu_method/de/eu_"+target_model+"all_de.npy", entropies_x_sample)
        #np.save("output/food101/eu_method/de/pred_"+target_model+"all_de.npy", pred_x_sample)
